# Code Drops Tema Light Dark Com React Styled Components E Typescript

Código desenvolvido no Code/Drops #16 - Tema light/dark com React, Styled Components e TypeScript

## Início

- Modelo usando como template o typescript

```bash
$ create-react-app themeswitcher --template=typescript
```

## Demonstração

![Demonstração](images/teste.gif "Demonstração")

### Dependências :

```bash
/* Dependência para criar componente de Switch */
$ yarn add react-switch

/* Instala o Styled Component */
$ yarn add styled-components
$ yarn add @types/styled-components -D

/* Dependência para lidar com Cores */
$ yarn add polished

```

```
|-- themeswitcher
   |-- /public
   |-- /src
      |-- /components
         |-- /Header
            |-- index.tsx
            |-- styles.ts
      |-- /styles
         |-- /themes          // Pasta com os temas que serão chaveados
            |-- dark.ts
            |-- light.ts
         |-- global.ts        // Arquivo de Estilos Global
         |-- styled.d.ts      // Arquivo de Definições de Tipos
      |-- /utils
         |-- usePersistedState.ts

```
