/* Por causa do Typescripts que não consegue injetar as varáveis do
 * arquivo global de estilos é necessário criar este arquivo de definições
 * de tipos
 *
 */
import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    title: string;

    colors: {
      primary: string;
      secondary: string;

      background: string;
      text: string;
    };
  }
}
